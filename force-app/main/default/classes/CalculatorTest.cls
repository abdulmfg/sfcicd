@isTest
public with sharing class CalculatorTest {
    @isTest
    public static void testAdd(){
        Test.startTest();
        Integer val = Calculator.add();
        Test.stopTest();
        System.assertEquals(val,4);
    }
    @isTest
    public static void testSub(){
        Test.startTest();
        Integer val = Calculator.sub();
        Test.stopTest();
        System.assertEquals(val,0);
    }
}